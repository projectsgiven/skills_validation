package fr.teknoup.skills_validation.payload.response;

public class MessageResponse {

    private String message;

    private UploadFileResponse uploadFileResponse;
    public MessageResponse(String message){
        this.message = message;
    }

    public MessageResponse(String message, UploadFileResponse uploadFileResponse) {
        this.message = message;
        this.uploadFileResponse = uploadFileResponse;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public UploadFileResponse getUploadFileResponse() {
        return uploadFileResponse;
    }

    public void setUploadFileResponse(UploadFileResponse uploadFileResponse) {
        this.uploadFileResponse = uploadFileResponse;
    }
}
