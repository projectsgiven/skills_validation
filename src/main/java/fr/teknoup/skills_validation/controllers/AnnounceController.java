package fr.teknoup.skills_validation.controllers;

import fr.teknoup.skills_validation.model.entity.Announcement;
import fr.teknoup.skills_validation.payload.response.MessageResponse;
import fr.teknoup.skills_validation.payload.validation.AnnouncementRequest;
import fr.teknoup.skills_validation.service.AnnouncementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

@RestController("announce")
@RequestMapping("/api/v1/announce")
public class AnnounceController {
    @Autowired
    AnnouncementService announcementService;

    @RequestMapping(method = RequestMethod.POST, value = "/addOne")
    public ResponseEntity<MessageResponse> addAnnounce(AnnouncementRequest announce) {
        MessageResponse newAnnounce = null;
        try {
            newAnnounce = announcementService.createAnnouncement(announce);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new ResponseEntity<>(newAnnounce, HttpStatus.CREATED);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/{id}")
    public ResponseEntity<Announcement> updateAnnounce( @PathVariable Integer id, @RequestBody AnnouncementRequest announce) {
        Optional<Announcement> announcement = announcementService.updateAnnouncement(id,announce) ;
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/all")
    public ResponseEntity<List<Announcement>> getAllAnnounces () {
        List<Announcement> newAnnounce = announcementService.getAllAnnouncements();
        return new ResponseEntity<>(newAnnounce, HttpStatus.OK);
    }


    @RequestMapping(method = RequestMethod.GET, value = "/{id}")
    public ResponseEntity<Announcement> getAnnounceById (@PathVariable("id") Integer id) {
        Announcement newAnnounce = announcementService.getASingleAnnouncement(id);
        return new ResponseEntity<>(newAnnounce, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
    public ResponseEntity<?> deleteAnnounce(@PathVariable("id") Integer id) {
        announcementService.deleteAnnouncement(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
