package fr.teknoup.skills_validation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SkillsValidationApplication {

    public static void main(String[] args) {
        SpringApplication.run(SkillsValidationApplication.class, args);
    }

}
