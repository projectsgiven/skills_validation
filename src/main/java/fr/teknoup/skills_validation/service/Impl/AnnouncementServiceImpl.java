package fr.teknoup.skills_validation.service.Impl;

import fr.teknoup.skills_validation.exception.ResourceNotFoundException;
import fr.teknoup.skills_validation.model.entity.Announcement;
import fr.teknoup.skills_validation.model.repository.AnnouncementRepository;
import fr.teknoup.skills_validation.payload.response.MessageResponse;
import fr.teknoup.skills_validation.payload.response.UploadFileResponse;
import fr.teknoup.skills_validation.payload.validation.AnnouncementRequest;
import fr.teknoup.skills_validation.service.AnnouncementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class AnnouncementServiceImpl implements AnnouncementService {

    @Autowired
    private FileStorageService fileStorageService;

    @Autowired
    private AnnouncementRepository announcementRepository;

    @Override
    public MessageResponse createAnnouncement(AnnouncementRequest announcementRequest) throws IOException {
        Announcement newAnnounce=new Announcement();
        newAnnounce.setTitle(announcementRequest.getTitle());
        newAnnounce.setDescription(announcementRequest.getDescription());
        String fileName = fileStorageService.storeFile(announcementRequest.getPicture());
        newAnnounce.setDescription(announcementRequest.getDescription());
        newAnnounce.setCreatedAt(new Date());
        try {
            newAnnounce.setPicture(fileStorageService.loadFileAsResource(fileStorageService.storeFile(announcementRequest.getPicture())).getURI().getPath().replace("/C:", "C:"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        announcementRepository.save(newAnnounce);

        return new MessageResponse("Announce created successfully", new UploadFileResponse(fileName, (fileStorageService.loadFileAsResource(fileStorageService.storeFile(announcementRequest.getPicture())).getURI().getPath()).replace("/C:", "C:"),
                announcementRequest.getPicture().getContentType(), announcementRequest.getPicture().getSize()));
    }

    @Override
    public Optional<Announcement> updateAnnouncement(Integer  id, AnnouncementRequest announcementRequest) {
        Optional<Announcement> announcement = announcementRepository.findById(id);
        if (announcement.isEmpty()){
            throw new ResourceNotFoundException("Announce  id" +id);
        }
        else
            announcement.get().setTitle(announcementRequest.getTitle());
        announcement.get().setDescription(announcementRequest.getDescription());
        announcement.get().getUpdateAt(new Date());
        try {
            announcement.get().setPicture(fileStorageService.loadFileAsResource(fileStorageService.storeFile(announcementRequest.getPicture())).getURI().getPath());
        } catch (IOException e) {
            e.printStackTrace();
        }
        announcementRepository.save(announcement.get());
        return announcement;
    }

    @Override
    public void  deleteAnnouncement(Integer  id) {
        if (announcementRepository.getById(id).getId().equals(id)){
            announcementRepository.deleteById(id);
        }
        else throw new ResourceNotFoundException("Announce  id" +id);
    }

    @Override
    public Announcement getASingleAnnouncement(Integer  id) {
        return announcementRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Announce id" +id));
    }

    @Override
    public List<Announcement> getAllAnnouncements() {
        return announcementRepository.findAll();
    }
}
