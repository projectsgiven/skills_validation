package fr.teknoup.skills_validation.service;

import fr.teknoup.skills_validation.model.entity.Announcement;
import fr.teknoup.skills_validation.payload.response.MessageResponse;
import fr.teknoup.skills_validation.payload.response.UploadFileResponse;
import fr.teknoup.skills_validation.payload.validation.AnnouncementRequest;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

@Component
public interface AnnouncementService {
    MessageResponse createAnnouncement(AnnouncementRequest announcementRequest) throws IOException;
    Optional<Announcement> updateAnnouncement(Integer  id, AnnouncementRequest announcementRequest);
    void  deleteAnnouncement(Integer  id);
    Announcement getASingleAnnouncement(Integer  id);
    List<Announcement> getAllAnnouncements();
}
